<?php
/**
 * Template Name: Menu Beverages Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marias mexican restaurant
 */

get_header();
?>
	<div id="menu-beverages" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<?php marias_post_thumbnail(); ?>

			<div class="entry-content">
				<ul class="menu-submenu">
					<li><a href="#beverages">Beverages</a></li>
					<li><a href="#margaritas">Margaritas</a></li>
					<li><a href="#wine">Wine</a></li>
					<li><a href="#beer">Beer</a></li>
					<li><a href="#other-drinks">Other Drinks</a></li>
				</ul>
				<?php
					the_content();
				?>

				<?php $beverages_query = new WP_Query(array(
						'category_name' => 'beverages',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $beverages_query->have_posts() ) : ?>
				<div id="beverages"></div>
				<h2>Beverages</h2>
				<ul class="menu-items">
					<?php
					while ( $beverages_query->have_posts() ) : $beverages_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $margaritas_query = new WP_Query(array(
						'category_name' => 'margaritas',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $margaritas_query->have_posts() ) : ?>
				<div id="margaritas"></div>
				<h2>Margaritas</h2>
				<ul class="menu-items">
					<?php
					while ( $margaritas_query->have_posts() ) : $margaritas_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $wine_query = new WP_Query(array(
						'category_name' => 'wine',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $wine_query->have_posts() ) : ?>
				<div id="wine"></div>
				<h2>Wine</h2>
				<ul class="menu-items">
					<?php
					while ( $wine_query->have_posts() ) : $wine_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $beer_query = new WP_Query(array(
						'category_name' => 'beer',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $beer_query->have_posts() ) : ?>
				<div id="beer"></div>
				<h2>Beer</h2>
				<ul class="menu-items">
					<?php
					while ( $beer_query->have_posts() ) : $beer_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $other_drinks_query = new WP_Query(array(
						'category_name' => 'other-drinks',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $other_drinks_query->have_posts() ) : ?>
				<div id="other-drinks"></div>
				<h2>Other Drinks</h2>
				<ul class="menu-items">
					<?php
					while ( $other_drinks_query->have_posts() ) : $other_drinks_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>
			</div>
		</main>
	</div>
<?php
get_footer();
