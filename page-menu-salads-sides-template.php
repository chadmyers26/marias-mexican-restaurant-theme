<?php
/**
 * Template Name: Menu Salads and Sides Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marias mexican restaurant
 */

get_header();
?>
	<div id="menu-salads-sides" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<?php marias_post_thumbnail(); ?>

			<div class="entry-content">
				<?php $salads_query = new WP_Query(array(
						'category_name' => 'salads',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $salads_query->have_posts() ) : ?>
				<h2>Salads</h2>
				<ul class="menu-items">
					<?php
					while ( $salads_query->have_posts() ) : $salads_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $sides_query = new WP_Query(array(
						'category_name' => 'sides',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $sides_query->have_posts() ) : ?>
				<h2>Side Orders<br />A La Carte</h2>
				<p class="notes">Add cheese sauce 1 burrito or enchilada $.50<br />2 burritos or 3 enchiladas $.75</p>
				<ul class="menu-items">
					<?php
					while ( $sides_query->have_posts() ) : $sides_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>
			</div>
		</main>
	</div>
<?php
get_footer();
