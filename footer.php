<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Marias
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="left-logo-area">
			<?php
				if(is_active_sidebar('footer-left')){
				dynamic_sidebar('footer-left');
				}
			?>
		</div>
		<div class="center-area">
			<?php
				if(is_active_sidebar('footer-center')){
				dynamic_sidebar('footer-center');
				}
			?>
		</div>
		<div class="right-area">
			<?php
				if(is_active_sidebar('footer-right')){
				dynamic_sidebar('footer-right');
				}
			?>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
