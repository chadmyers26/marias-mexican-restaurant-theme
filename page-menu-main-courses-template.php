<?php
/**
 * Template Name: Menu Main Courses Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marias mexican restaurant
 */

get_header();
?>
	<div id="menu-main-courses" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<?php marias_post_thumbnail(); ?>

			<div class="entry-content">
				<ul class="menu-submenu">
					<li><a href="#lunch-only">Lunch Only</a></li>
					<li><a href="#super-burros">Super Burros</a></li>
					<li><a href="#combinations">Combinations</a></li>
					<li><a href="#vegetarian">Vegetarian</a></li>
					<li><a href="#house-specials">House Specials</a></li>
					<li><a href="#seafood">Seafood</a></li>
					<li><a href="#steak">Steak</a></li>
					<li><a href="#chicken">Chicken</a></li>
				</ul>

				<?php $lunch_only_query = new WP_Query(array(
						'category_name' => 'lunch-only',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $lunch_only_query->have_posts() ) : ?>
				<div id="lunch-only"></div>
				<h2>Lunch Only</h2>
				<p class="notes">Monday - Saturday 11:00 a.m. to 2:30 p.m.</p>
				<p class="notes">Add cheese sauce $.75</p>
				<ul class="menu-items">
					<?php
					while ( $lunch_only_query->have_posts() ) : $lunch_only_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $burros_query = new WP_Query(array(
						'category_name' => 'super-burros',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $burros_query->have_posts() ) : ?>
				<div id="super-burros"></div>
				<h2>Super Burros</h2>
				<p class="notes">All Burros are served with lettuce, tomato, sour cream, cheese, onions, beans and rice.</p>
				<ul class="menu-items">
					<?php
					while ( $burros_query->have_posts() ) : $burros_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $combo_query = new WP_Query(array(
						'category_name' => 'combinations',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $combo_query->have_posts() ) : ?>
				<div id="combinations"></div>
				<h2>Combinations</h2>
				<p class="notes">Combinations include rice or black beans or charro beans or refried beans.</p>
				<ul class="menu-items">
					<?php
					while ( $combo_query->have_posts() ) : $combo_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $vegetarian_query = new WP_Query(array(
						'category_name' => 'vegetarian',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $vegetarian_query->have_posts() ) : ?>
				<div id="vegetarian"></div>
				<h2>Vegetarian</h2>
				<p class="notes">Add cheese sauce $.99</p>
				<ul class="menu-items">
					<?php
					while ( $vegetarian_query->have_posts() ) : $vegetarian_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $house_query = new WP_Query(array(
						'category_name' => 'house-specials',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $house_query->have_posts() ) : ?>
				<div id="house-specials"></div>
				<h2>House Specials</h2>
				<p class="notes">Add cheese sauce $.99</p>
				<ul class="menu-items">
					<?php
					while ( $house_query->have_posts() ) : $house_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $fajitas_specialties_query = new WP_Query(array(
						'category_name' => 'fajitas-specialties',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $fajitas_specialties_query->have_posts() ) : ?>
				<h2>More House Fajitas Specialties</h2>
				<p class="notes">Every Fajitas Dinner is served in a sizzling skillet with Spanish rice, fried beans, salad (guacamole, sour cream, lettuce and tomatoes) and flour tortillas.</p>
				<ul class="menu-items">
					<?php
					while ( $fajitas_specialties_query->have_posts() ) : $fajitas_specialties_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $seafood_query = new WP_Query(array(
						'category_name' => 'seafood',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $seafood_query->have_posts() ) : ?>
				<div id="seafood"></div>
				<h2>Seafood</h2>
				<ul class="menu-items">
					<?php
					while ( $seafood_query->have_posts() ) : $seafood_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $steak_query = new WP_Query(array(
						'category_name' => 'steak',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $steak_query->have_posts() ) : ?>
				<div id="steak"></div>
				<h2>Steak</h2>
				<ul class="menu-items">
					<?php
					while ( $steak_query->have_posts() ) : $steak_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>

				<?php $chicken_query = new WP_Query(array(
						'category_name' => 'chicken_query',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $chicken_query->have_posts() ) : ?>
				<div id="chicken"></div>
				<h2>Chicken</h2>
				<ul class="menu-items">
					<?php
					while ( $chicken_query->have_posts() ) : $chicken_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>
			</div>
		</main>
	</div>
<?php
get_footer();
