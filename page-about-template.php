<?php
/**
 * Template Name: About Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marias mexican restaurant
 */

get_header();
?>
	<div id="about" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

			<div class="philosophies">
				<?php
					if(is_active_sidebar('philosophy-left')){
						dynamic_sidebar('philosophy-left');
					}
				?>
				<?php
					if(is_active_sidebar('philosophy-center')){
					dynamic_sidebar('philosophy-center');
					}
				?>
				<?php
					if(is_active_sidebar('philosophy-right')){
					dynamic_sidebar('philosophy-right');
					}
				?>
			</div>
		</main>
	</div>
<?php
get_footer();
