<?php
/**
 * Marias functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Marias
 */

if ( ! function_exists( 'marias_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function marias_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Marias, use a find and replace
		 * to change 'marias' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'marias', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'marias' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'marias_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'marias_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function marias_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'marias_content_width', 640 );
}
add_action( 'after_setup_theme', 'marias_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function marias_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'marias' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'marias' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'marias_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function marias_scripts() {
	wp_enqueue_style( 'marias-style', get_stylesheet_uri() );
	wp_enqueue_style( 'marias-theme-styles', get_theme_file_uri( '/assets/css/marias.css' ));
	wp_enqueue_script( 'marias-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'marias-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'marias_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

register_sidebar( array(
	'name' => 'Footer Left',
	'id' => 'footer-left',
	'description' => 'Appears in the footer area on the left hand side',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'name' => 'Footer Center',
	'id' => 'footer-center',
	'description' => 'Appears in the footer area in the center',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'name' => 'Footer Right',
	'id' => 'footer-right',
	'description' => 'Appears in the footer area on the right side',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'name' => 'Welcome Image Sidebar',
	'id' => 'welcome-image-sidebar',
	'description' => 'Appears on the home page in the welcome area',
	'before_title' => '<div class="image">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Welcome Text Sidebar',
	'id' => 'welcome-text-sidebar',
	'description' => 'Appears on the home page in the welcome area',
	'before_title' => '<div class="text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Service Image 1 Sidebar',
	'id' => 'service-image-1-sidebar',
	'description' => 'Appears on the home page in the friendly service area',
	'before_title' => '<div class="image-1">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Service Image 2 Sidebar',
	'id' => 'service-image-2-sidebar',
	'description' => 'Appears on the home page in the friendly service area',
	'before_title' => '<div class="image-2">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Service Image 3 Sidebar',
	'id' => 'service-image-3-sidebar',
	'description' => 'Appears on the home page in the friendly service area',
	'before_title' => '<div class="image-3">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Service Image 4 Sidebar',
	'id' => 'service-image-4-sidebar',
	'description' => 'Appears on the home page in the friendly service area',
	'before_title' => '<div class="image-4">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Whats On The Menu Image 1 Sidebar',
	'id' => 'what-on-the-menu-img1-sidebar',
	'description' => 'Appears on the home page in the Whats On The Menu area',
	'before_title' => '<div class="what-on-the-menu-img1">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Whats On The Menu Image 2 Sidebar',
	'id' => 'what-on-the-menu-img2-sidebar',
	'description' => 'Appears on the home page in the Whats On The Menu area',
	'before_title' => '<div class="what-on-the-menu-img2">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Whats On The Menu Image 3 Sidebar',
	'id' => 'what-on-the-menu-img3-sidebar',
	'description' => 'Appears on the home page in the Whats On The Menu area',
	'before_title' => '<div class="what-on-the-menu-img3">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Whats On The Menu Image 4 Sidebar',
	'id' => 'what-on-the-menu-img4-sidebar',
	'description' => 'Appears on the home page in the Whats On The Menu area',
	'before_title' => '<div class="what-on-the-menu-img4">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Testimonials Sidebar',
	'id' => 'testimonials-sidebar',
	'description' => 'Appears on the home page in the testimonials area',
	'before_title' => '<div class="testimonial-slider">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Room Image Sidebar',
	'id' => 'room-image-sidebar',
	'description' => 'Appears on the home page in the room area',
	'before_title' => '<div class="image">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Room Text Sidebar',
	'id' => 'room-text-sidebar',
	'description' => 'Appears on the home page in the room area',
	'before_title' => '<div class="text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Philosophy Left Sidebar',
	'id' => 'philosophy-left',
	'description' => 'Appears on the about page in the philosophy area',
	'before_title' => '<div class="text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Philosophy Center Sidebar',
	'id' => 'philosophy-center',
	'description' => 'Appears on the about page in the philosophy area',
	'before_title' => '<div class="text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Philosophy Right Sidebar',
	'id' => 'philosophy-right',
	'description' => 'Appears on the about page in the philosophy area',
	'before_title' => '<div class="text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Contact Us Sidebar',
	'id' => 'contact-us',
	'description' => 'Appears on the about page in the Contact Us area',
	'before_title' => '<div class="contact-us-text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Restaurant Hours Sidebar',
	'id' => 'restaurant-hours',
	'description' => 'Appears on the about page in the Restaurant Hours area',
	'before_title' => '<div class="restaurant-hours-text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Contact Form Sidebar',
	'id' => 'contact-form',
	'description' => 'Appears on the about page in the Contact Form area',
	'before_title' => '<div class="contact-form-text">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Menu Options Area',
	'id' => 'menu-options',
	'description' => 'Appears on the menu page',
	'before_title' => '<div class="menu-options">',
	'after_title' => '</div>',
) );