<?php
/**
 * Template Name: Contact Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marias mexican restaurant
 */

get_header();
?>
	<div id="contact" class="content-area">
		<main id="main" class="site-main">
			<div class="contact-us">
				<div class="contact">
					<?php
						if(is_active_sidebar('contact-us')){
							dynamic_sidebar('contact-us');
						}
					?>
				</div>
				<div class="hours">
					<?php
						if(is_active_sidebar('restaurant-hours')){
							dynamic_sidebar('restaurant-hours');
						}
					?>
				</div>
			</div>
			<div class="google-map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3069.1418307141903!2d-82.60287278463103!3d39.713995179453086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88478af9e8ca4a45%3A0xac458a2f1de7913b!2s129+E+Main+St%2C+Lancaster%2C+OH+43130!5e0!3m2!1sen!2sus!4v1550952544514" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="contact-form">
				<div class="contact-wrapper">
					<div class="get-in-touch">
						<h2>GET IN TOUCH</h2>
						<p>We would love to hear from you!<br />
						We take great price in our restaurant, our food, and our service! Contact us to book one of our dinning rooms, or rent out our party room!</p>
					</div>
					<?php
						if(is_active_sidebar('contact-form')){
							dynamic_sidebar('contact-form');
						}
					?>
				</div>
			</div>
		</main>
	</div>
<?php
get_footer();
