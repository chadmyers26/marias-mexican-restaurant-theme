<?php
/**
 * Template Name: Home Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marias mexican restaurant
 */

get_header();
?>
	<div id="home" class="content-area">
		<main id="main" class="site-main">
			<section class="slider">
				<?php echo do_shortcode('[metaslider id="34"]'); ?>
			</section>
			<section class="welcome">
				<h2>Welcome to Maria's</h2>
				<div class="welcome-content">
					<?php
						if(is_active_sidebar('welcome-image-sidebar')){
						dynamic_sidebar('welcome-image-sidebar');
						}
					?>
					<?php
						if(is_active_sidebar('welcome-text-sidebar')){
						dynamic_sidebar('welcome-text-sidebar');
						}
					?>
				</div>
			</section>
			<section class="service">
				<h2>Friendly Service</h2>
				<div class="service-content">
					<?php
						if(is_active_sidebar('service-image-1-sidebar')){
						dynamic_sidebar('service-image-1-sidebar');
						}
					?>
					<?php
						if(is_active_sidebar('service-image-2-sidebar')){
						dynamic_sidebar('service-image-2-sidebar');
						}
					?>
					<?php
						if(is_active_sidebar('service-image-3-sidebar')){
						dynamic_sidebar('service-image-3-sidebar');
						}
					?>
					<?php
						if(is_active_sidebar('service-image-4-sidebar')){
						dynamic_sidebar('service-image-4-sidebar');
						}
					?>
				</div>
			</section>
			<section class="party-room-rental">
				<h2>Party Room Rental</h2>
				<div class="party-room-rental-content">
					<?php
						if(is_active_sidebar('room-text-sidebar')){
							dynamic_sidebar('room-text-sidebar');
						}
					?>
					<?php
						if(is_active_sidebar('room-image-sidebar')){
						dynamic_sidebar('room-image-sidebar');
						}
					?>
				</div>
			</section>
			<section class="what-on-the-menu">
				<h2>What's on the menu</h2>
				<div class="what-on-the-menu-content">
					<?php
						if(is_active_sidebar('what-on-the-menu-img1-sidebar')){
						dynamic_sidebar('what-on-the-menu-img1-sidebar');
						}
					?>

					<?php
						if(is_active_sidebar('what-on-the-menu-img2-sidebar')){
						dynamic_sidebar('what-on-the-menu-img2-sidebar');
						}
					?>

					<?php
						if(is_active_sidebar('what-on-the-menu-img3-sidebar')){
						dynamic_sidebar('what-on-the-menu-img3-sidebar');
						}
					?>

					<?php
						if(is_active_sidebar('what-on-the-menu-img4-sidebar')){
						dynamic_sidebar('what-on-the-menu-img4-sidebar');
						}
					?>
				</div>
			</section>
		</main>
	</div>
<?php
get_footer();
