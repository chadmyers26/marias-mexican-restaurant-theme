<?php
/**
 * Template Name: Menu Desserts Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package marias mexican restaurant
 */

get_header();
?>
	<div id="menu-desserts" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<?php marias_post_thumbnail(); ?>

			<div class="entry-content">
				<?php $desserts_query = new WP_Query(array(
						'category_name' => 'desserts',
						'posts_per_page' => -1
					));
				?>
				<?php if ( $desserts_query->have_posts() ) : ?>
				<ul class="menu-items">
					<?php
					while ( $desserts_query->have_posts() ) : $desserts_query->the_post();
					?>
						<li>
							<h2><?php the_title(); ?></h2>
							<p><?php the_content(); ?></p>
						</li>
					<?php endwhile; ?>
				</ul>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>
			</div>
		</main>
	</div>
<?php
get_footer();
